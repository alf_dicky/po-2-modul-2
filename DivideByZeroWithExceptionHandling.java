import java.lang.*;
import java.util.InputMismatchException;
import java.util.Scanner;
public class DivideByZeroWithExceptionHandling{
    public static int pembagian(int bil, int pbg) throws ArithmeticException{
        return bil/pbg;
    }

    public static void main(String[] args){
        Scanner myscanner = new Scanner(System.in);
        boolean continueLoop = true;

        do{
            try{
                System.out.print("Please enter an integer numerator   = ");
                int numerator = myscanner.nextInt();
                System.out.print("Please enter an integer denominator = ");
                int denominator = myscanner.nextInt();

                int result = pembagian(numerator,denominator);
                System.out.print("Result : "+numerator+"/"+denominator+" = "+result);
                continueLoop = false;
            }catch(InputMismatchException e){
                System.err.println("Exception : "+e);
                myscanner.nextLine();
                System.out.println("You must enter integers. please try again.");
            }catch(ArithmeticException arithmeticException){
                System.err.println("Exception = "+arithmeticException);
                System.out.println("Zero is an invalid denominator. please try again.%n%n");
            }
        }while(continueLoop);
    }
}