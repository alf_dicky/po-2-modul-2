public class Account{
    private int accountNumber;
    private int currentBalance;

    public Account(int an, int cb){
        accountNumber = an;
        currentBalance = cb;
    }

    public void credit(int amt){
        currentBalance = currentBalance + amt;
    }

    public void debit(int amt) throws ArithmeticException{
        int tempBalance = currentBalance - amt;
        if(tempBalance<0){
            int i =1/0;
            System.out.println(i);
        }
        currentBalance = tempBalance;
    }

    public int getCurrentBalance(){
        return currentBalance;
    }
}