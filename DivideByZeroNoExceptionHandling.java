import java.util.Scanner;

public class DivideByZeroNoExceptionHandling{
    public static int pembagian(int bil, int pbg){
        return bil/pbg;
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Masukan nilai pembilang = ");
        int numerator = scanner.nextInt();
        System.out.print("Masukan nilai penyebut (pembagi) = ");
        int denominator = scanner.nextInt();

        int result = pembagian(numerator,denominator);
        System.out.print("Result : "+numerator+"/"+denominator+" = "+result);
    }
}