import java.awt.Rectangle;
import java.lang.*;
public class WrongCasting{
    public static void main(String[] args){
        Rectangle rectangle = new Rectangle(10,10);
        try{
            Object str = (Object) rectangle;
        }catch(ClassCastException er){
            System.out.println("Error = "+er);
        }
        
        System.out.println("String str : "+str);
    }
}