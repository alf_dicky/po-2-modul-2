public class InternetBanking{
    public static void main(String[] args){
        Account xAcc = new Account(1, 1000);
        Account yAcc = new Account(2, 1200);
       try{
            FundTransfer.transferFunds(xAcc, yAcc, 2000);
        }catch(ArithmeticException er){
            System.out.println("error = "+er);
        }
        
        System.out.println("xAcc's current balance = "+xAcc.getCurrentBalance());
        System.out.println("yAcc's current balance = "+yAcc.getCurrentBalance());
        System.out.println("Completed execution of main method");
    }
}